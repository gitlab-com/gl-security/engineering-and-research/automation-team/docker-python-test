#!/usr/bin/env python3

import click


@click.command()
@click.argument('name')
def main(name):
	click.echo(f'Hello, {name}!')


if __name__ == '__main__':
	main()
